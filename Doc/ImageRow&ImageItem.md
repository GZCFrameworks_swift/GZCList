# ImageRow & ImageItem

图片展示Row，可设置图片预估比例、内容边距、圆角等，支持网络图片加载，可设置是否自动调整高度。采用了图片压缩缓存的策略，已解决大图片的卡顿问题，提升滑动的流畅度。

![](./ImageRow.gif)

## 属性

### 通用

> **estimatedSize**：图片预估比例
>
> **imageUrl**：网络图片地址字符串
>
> **image**：本地图片UIImage
>
> **loadingIndicatorType**：图片加载中的样式，`IndicatorType`类型，具体为：
>
> ```
> .none 默认没有菊花
> .activity 使用系统菊花
> .image(imageData: Data) 使用一张图片作为菊花，支持gif图
> .custom(indicator: Indicator) 使用自定义菊花，要遵循Indicator协议
> ```
>
> **loadFaildImage**：加载失败图片
>
> **contentMode**：图片填充模式
>
> **corners**：图片圆角的数组，`[CornerType]`类型，如果要四个角全部圆角，可使用`CornerType.all(10)`

### ImageRow

> **autoHeight**：是否自动高度，默认为true，如果设置为false，则会根据设置的预估比例设置高度

### ImageItem

> **autoSize**：是否自动调整尺寸，默认为true，如果设置为false，则会根据设置的预估比例设置尺寸

## 使用举例

### ImageRow

```
TableSection("ImageRow(图片展示)")
      <<< ImageRow() { row in
          row.imageUrl = ImageUrlsHelper.getRandomImage()
          row.corners = [.leftTop(10),.rightTop(10)]
          row.contentInsets = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
          row.estimatedSize = CGSize(width: 30, height: 40)
      }
      <<< ImageRow() { row in
          row.imageUrl = ImageUrlsHelper.getRandomImage()
          row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
          row.estimatedSize = CGSize(width: 30, height: 40)
      }
      <<< ImageRow() { row in
          row.imageUrl = ImageUrlsHelper.getRandomImage()
          row.corners = [.leftBottom(10),.rightBottom(10)]
          row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
          row.estimatedSize = CGSize(width: 30, height: 40)
      }
```

### ImageItem

```
CollectionSection("自动大小三列图片") { section in
    section.column = 3
    section.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    section.lineSpace = 5
    section.itemSpace = 5
}
    <<< ImageItem() { row in
          row.imageUrl = url
          row.corners = [.leftTop(10),.rightBottom(15)] 		// 左上、右下圆角
          row.autoSize = true																// 自动调整大小
          row.aspectRatio = CGSize(width: 1, height: 1) 		// 预设比例
          row.loadFaildImage = UIImage(named: "load_faild") // 加载失败图片
      }
```


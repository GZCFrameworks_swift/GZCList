# LabelRow & LabelItem

文字展示单元格

可展示标题和value，同时提供自定义标题、value样式和位置等属性

## 属性

### 整体设置 (基类中已有属性未列出)

> **verticalAlignment**: 竖直方向排列方式
>
> **spaceBetweenTitleAndValue**: 标题与值之间的间距

### 标题(title)样式

> **title**：标题内容文字
>
> **titlePosition**：位置(自动宽度/固定宽度)
>
> **titleFont**：字体
>
> **titleColor**：字体颜色
>
> **titleLines**: 行数
>
> **titleAlignment**：对齐方式
>
> **attributeTitle**：富文本标题，如果设置了，则会替换掉title显示这个

### 值(value)样式

>**value**：值的内容文字
>
>**valueFont**：字体
>
>**valueColor**：字体颜色
>
>**valueLines**：行数
>
>**valueAlignment**：对齐方式
>
>**attributeValue**：富文本value，如果设置了，则会替换掉value显示这个

## 使用举例

### LabelRow

```Swift
TableSection("LabelRow")
      <<< LabelRow("标题样式") { row in
          row.verticalAlignment = .top
          row.spaceBetweenTitleAndValue = 8

          row.titlePosition = .left
          row.titleFont = UIFont.boldSystemFont(ofSize: 15)
          row.titleColor = .darkText
          row.titleAlignment = .center

          row.valueColor = .blue
          row.valueAlignment = .left
          row.value = "value样式,然后这是一串比较长的字符串，我们看看能不能换行\n加个回车试试看"
      }
```

### LabelItem

```
CollectionSection("LabelItem") { section in
            section.lineSpace = 0
            section.column = 1
        }
            <<< LabelItem("标题样式") { row in
                row.verticalAlignment = .top
                row.spaceBetweenTitleAndValue = 8

                row.titlePosition = .left
                row.titleFont = UIFont.boldSystemFont(ofSize: 15)
                row.titleColor = .darkText
                row.titleAlignment = .center

                row.valueColor = .blue
                row.valueAlignment = .left
                row.value = "value样式,然后这是一串比较长的字符串，我们看看能不能换行\n加个回车试试看"
            }
```


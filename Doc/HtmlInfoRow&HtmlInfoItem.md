# HtmlInfoRow & HtmlInfoItem

带webview的row，用于展示html代码，会根据网页内容大小和用户设置自动调整最终展示大小

![](./HtmlInfoRow.png)

## 属性

> **value**：要显示的html代码内容
>
> **contentInsets**：四周边距
>
> **estimatedSize**：预设大小（如html代码为图片，且后台有给出图片大小，可使用此属性直接设置大小）

## 使用举例

### HtmlInfoRow

```
TableSection("HtmlInfoRow")
		<<< HtmlInfoRow() { row in
          row.value = "<img src = \"http://img.alicdn.com/imgextra/i3/124158638/O1CN01AlLzW02DgFnqvcWtB_!!124158638.jpg\"/>"
          row.estimatedSize = CGSize(width: 750, height: 730)
          row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
      }
```

### HtmlInfoItem

HtmlInfoItem的使用与HtmlInfoRow基本相同, ***不建议***将HtmlInfoItem添加到横向滚动的CollectionView中

```
CollectionSection("HtmlInfoItem") { section in
    section.lineSpace = 0
    section.column = 1
}
		<<< HtmlInfoItem() { row in
          row.value = "<img src = \"http://img.alicdn.com/imgextra/i3/124158638/O1CN01AlLzW02DgFnqvcWtB_!!124158638.jpg\"/>"
          row.estimatedSize = CGSize(width: 750, height: 730)
          row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
      }
```


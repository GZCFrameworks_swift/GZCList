# SwitchRow & SwitchItem

带switch控件的单元格，可展示标题和switch，同时提供自定义标题、switch样式和位置等属性

![](./SwitchRow.gif)

## 属性 

### 整体设置 (基类中已有属性未列出)

> **verticalAlignment**: 竖直方向排列方式

### 标题(title)样式

> **title**：标题内容文字
>
> **titlePosition**：位置(自动宽度/固定宽度)
>
> **titleFont**：字体
>
> **titleColor**：字体颜色
>
> **titleLines**: 行数
>
> **titleAlignment**：对齐方式
>
> **attributeTitle**：富文本标题，如果设置了，则会替换掉title显示这个

### 右侧滑块样式

>**value**：设置Switch控件的isOn状态，默认为nil
>
>**switchTintColor**：未选中的滑块背景色
>
>**switchOnTintColor**：选中的滑块背景色
>
>**switchSliderColor**：滑块颜色
>
>**switchSliderText**：未选中时滑块上添加的文字显示（建议单字）
>
>**switchOnSliderText**：选中时滑块上添加的文字显示（建议单字）
>
>**switchSliderTextColor**：滑块上添加的文字颜色，默认为透明

## 使用举例

### SwitchRow

```
TableSection("SwitchRow")
    <<< SwitchRow("设为默认") { row in
        row.value = true
    }.onChange({ (row) in
        /// 值改变的回调
        print(row.value ?? false)
    })
    <<< SwitchRow("自定义样式") { row in
        row.switchTintColor = .red
        row.switchOnTintColor = .blue
        row.switchSliderColor = .yellow
        row.switchSliderText = "关"
        row.switchOnSliderText = "开"
        row.switchSliderTextColor = .darkGray
        row.cellHeight = 60
    }
```

### SwitchItem

SwitchItem的使用与SwitchRow基本相同, ***不建议***将SwitchItem添加到横向滚动的CollectionView中

```
CollectionSection("SwitchItem") { section in
    section.lineSpace = 0
    section.column = 1
}
    <<< SwitchItem("设为默认") { row in
        row.value = true
    }.onChange({ (row) in
        /// 值改变的回调
        print(row.value ?? false)
    })
    <<< SwitchItem("自定义样式") { row in
        row.switchTintColor = .red
        row.switchOnTintColor = .blue
        row.switchSliderColor = .yellow
        row.switchSliderText = "关"
        row.switchOnSliderText = "开"
        row.switchSliderTextColor = .darkGray
        row.aspectHeight = 60
    }
```




# FoldTextRow & FoldTextItem

继承于FoldRow & FoldItem，用于快速创建文字内容的展开单元格，自定义左侧视图、折叠按钮样式的方法参考这里[FoldRow&FoldItem](./Doc/FoldRow&FoldItem.md)

![](./FoldText.gif)

## 属性 (父类中已有属性未列出)

### 文字内容样式

> **title**：row的title将会直接设置为展示的文本内容
>
> **fontOfText**：文本字体
>
> **colorOfText**：文本颜色
>
> **alignmentOfText**：文本对齐方式
>
> **attributeText**：富文本内容，该设置会替换title作为展示内容

## 使用举例

### FoldTextRow

```
TableSection("FoldTextRow(可折叠的文字)")
    <<< FoldTextRow("FoldTextRow是可折叠的文字展示Row，当长度超过指定的foldHeight时，会自动显示展开按钮，展开后可以收起，然后下面是回车\n看下是不是可以") { row in
        row.foldHeight = 20
    }
    <<< FoldTextRow() { row in
        row.foldHeight = 20
        let attr = NSMutableAttributedString(string: "这行来测试一下富文本内容的展示，这是红色的字,\n以及左侧自定义View的使用\n这行来测试一下富文本内容的展示,\n以及左侧自定义View的使用")
        attr.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: 16, length: 6))
        row.attributeText = attr

        let photoImage = UIImageView()
        photoImage.layer.cornerRadius = 15
        photoImage.clipsToBounds = true
        photoImage.kf.setImage(with: URL(string: ImageUrlsHelper.getRandomImage()))
        row.leftView = photoImage
        row.leftViewSize = CGSize(width: 30, height: 30)
    }
    <<< FoldTextRow("这行来测试一下自定义的展开不能收起\n这行来测试一下自定义的展开不能收起\n这行来测试一下自定义的展开不能收起\n这行来测试一下自定义的展开不能收起\n这行来测试一下自定义的展开不能收起\n这行来测试一下自定义的展开不能收起\n这行来测试一下自定义的展开不能收起\n这行来测试一下自定义的展开不能收起") { row in
        row.foldHeight = 40
        let foldView = DemoFoldButton()
        foldView.showCloseWhenOpend = false
        row.foldOpenView = DemoFoldButton()
        row.openViewPosition = .cover
    }
```



### FoldTextItem

FoldTextItem的使用与FoldTextRow基本相同, 目前不建议将FoldTextItem添加到横向滚动的CollectionView中

```
CollectionSection("FoldTextItem(可折叠的文字)") { section in
    section.lineSpace = 0
    section.column = 1
}
      <<< FoldTextItem("FoldTextRow是可折叠的文字展示Row，当长度超过指定的foldHeight时，会自动显示展开按钮，展开后可以收起，然后下面是回车\n看下是不是可以") { row in
          row.foldHeight = 20
      }
      <<< LineItem() { row in
          row.contentInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
          row.lineWidth = 30
          row.cornerRadius = 15
      }
      <<< FoldTextItem() { row in
          row.foldHeight = 20
          let attr = NSMutableAttributedString(string: "这行来测试一下富文本内容的展示，这是红色的字,\n这行来测试一下富文本内容的展示\n这行来测试一下富文本内容的展示")
          attr.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: 16, length: 6))
          row.attributeText = attr
      }
```
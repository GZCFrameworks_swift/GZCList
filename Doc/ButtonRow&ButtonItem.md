# ButtonRow & ButtonItem

按钮行（整个行为一个按钮），点击可以任意操作（如点击跳转到新的界面）

支持自定义标题样式、右侧箭头样式，同时可添加左侧图标，以及右侧箭头前的自定义View

文字内容展示为row.title，如需修改可以继承对应的`ButtonRowOf<T: Equatable>`或`ButtonItemOf<T: Equatable>`并在super.customUpdateCell()之后设置cell.titleLabel.text为想要的值

![](./Button.gif)

## 属性 (基类中已有属性未列出)

### 箭头样式

> **arrowType**：箭头样式，枚举类型。
>
> * ButtonRow*包含`不带箭头(.none)/系统自带(.system)和自定义(.custom(image:size:))`三种样式
> * ButtonItem*包含`不带箭头(.none)和自定义(.custom(image:size:))`两种样式

### 左侧图标

> **iconImage**：左侧图标图片
>
> **iconSize**：左侧图标大小

### 标题

>**fontOfTitle**：字体
>
>**colorOfTitle**：颜色
>
>**alignmentOfTitle**：对齐方式

### 右侧自定义控件（箭头与标题之间靠箭头）

>**rightView**：右侧自定义控件
>
>rightViewSize：右侧自定义控件大小

### 间距设置

>**spaceBetweenIconAndTitle**：左侧图标与标题之间的间距
>
>**spaceBetweenTitleAndRightView**：标题与右侧自定义控件之间的间距
>
>**spaceBetweenRightViewAndArrow**：右侧自定义控件与箭头之间的间距

### 点击关联跳转

> **presentationMode**：定义了点击如何后跳转控制器的属性，可以不传

## 使用举例

### ButtonRow

````
TableSection("ButtomRow")
      <<< ButtonRow("点击跳转(show)") { row in
          row.value = "传值1"
          /// 自动选择push和present
          row.presentationMode = .show(controllerProvider: .callback(builder: { () -> UIViewController in
          		/// vc可以是任意的UIViewController
              let vc = PresentViewController<ButtonRow>()
              vc.modalPresentationStyle = .fullScreen
              vc.row = row
              return vc
          }), onDismiss: { (vc) in
          		/// 要使用此方法，需要上面的vc实现 TypedRowControllerType 协议，并在返回的方法中调用
          		/// onDismissCallback?(self)
              vc.dismiss(animated: true)
          })
      }
      <<< ButtonRow("点击跳转(popover)") { [weak self] row in
          row.value = "传值3"
          /// 指定popover (气泡)
          row.presentationMode = .popover(controllerProvider: .callback(builder: { () -> UIViewController in
              let vc = PresentViewController<ButtonRow>()
              vc.preferredContentSize = CGSize(width: 150, height: 150)
              vc.modalPresentationStyle = .popover
              /// ***必须实现delegate中的adaptivePresentationStyle方法***
              /// ***这里的self一定要用weak修饰，否则会造成循环引用***
              if let weakSelf = self {
                  vc.popoverPresentationController?.delegate = weakSelf
              }
              vc.popoverPresentationController?.sourceView = row.cell
              vc.popoverPresentationController?.permittedArrowDirections = .any
              vc.popoverPresentationController?.backgroundColor = .green
              vc.row = row
              return vc
          }), onDismiss: { (vc) in
              vc.dismiss(animated: true)
          })
      }
````



### ButtonItem

ButtonItem的使用与ButtonRow基本相同，修改类名即可添加到对应的Section中：

```
CollectionSection("Section")
      <<< ButtonItem("点击跳转(show)") {[weak self] row in
        row.value = "传值1"
        /// 设置圆角为高度的一半
        row.cornerScale = 0.5
        /// 设置边框宽度
        row.borderWidth = 1
        /// 设置正常颜色
        row.titleColor = .black
        row.contentBgColor = UIColor(white: 0.9, alpha: 1.0)
        row.borderColor = UIColor(white: 0.5, alpha: 1.0)
        /// 设置高亮颜色
        row.titleHighlightColor = .white
        row.highlightContentBgColor = UIColor(red: 59/255.0, green: 138/255.0, blue: 250/255.0, alpha: 1)
        row.highlightBorderColor = UIColor(red: 59/255.0, green: 138/255.0, blue: 250/255.0, alpha: 1)
        /// 自动选择push和present
        row.presentationMode = .show(controllerProvider: .callback(builder: { () -> UIViewController in
            let vc = ItemPresentViewController<ButtonItem>()
            vc.modalPresentationStyle = .fullScreen
            vc.row = row
            return vc
        }), onDismiss: { (vc) in
            if vc.navigationController != nil {
                vc.navigationController?.popViewController(animated: true)
            } else {
                vc.dismiss(animated: true)
            }
        })
        if self?.scrollDirection == .horizontal {
            row.contentInsets = UIEdgeInsets(top: 10, left: 7, bottom: 10, right: 5)
        }
    }
```




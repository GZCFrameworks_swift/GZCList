# LineRow & LineItem

定义好的分割线单元格，可自定义线的宽度、圆角、内容边距、线的颜色以及背景色，使用比较简单。

![](./LineRow.png)

## 属性

> **lineColor**：线条颜色
>
> **lineRadius**：线条圆角
>
> **lineWidth**：线条宽度
>
> **contentInsets**：线条四周的边距
>
> **backgroundColor**：背景色

## 使用举例

### LineRow

```
TableSection("LineRow")
    <<< LabelRow("LineRow是定义好的分割线Row，可自定义分割线宽度(lineWidth)、圆角、内容边距、线的颜色，默认高度为0.5，可以作为普通的分割线，如：")
    <<< LineRow() { row in
        row.contentInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        row.lineWidth = 30
        row.cornerRadius = 15
    }
    <<< LabelRow("也可以将lineColor和backgroundColor设置为透明达到分块的效果，如：")
    <<< LineRow() { row in
        row.lineColor = .clear
        row.backgroundColor = .clear
        row.lineWidth = 15
    }
```

### LineItem

```
CollectionSection("LineItem(分割线)") { section in
    section.lineSpace = 0
    section.column = 1
}
    <<< LineItem() { row in
        row.contentInsets = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15)
        row.lineWidth = 30
        row.cornerRadius = 15
    }
    <<< LineItem() { row in
        row.contentInsets = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 0)
        row.lineWidth = 3
        row.cornerRadius = 1.5
    }
    <<< LineItem() { row in
        row.contentInsets = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15)
        row.lineColor = .red
    }
    <<< LineItem() { row in
        row.contentInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
```


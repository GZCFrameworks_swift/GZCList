# FoldRow & FoldItem

可折叠/展开的Row，支持自定义折叠内容控件、左侧view以及折叠按钮样式

![](FoldRow.gif)



## 属性 (基类中已有属性未列出)

## Row

**FoldRow / FoldItem** 为可直接使用的单元格类型，不可继承，可通过设置相关位置的view来修改样式。

**_FoldRowOf<C: FoldCell> / _FoldItemOf<C: FoldCollectionCell>** 为可继承的单元格类型，可继承并在`customUpdateCell()`方法中设置自定义的view的相关值（设置完成后记得调用**`super.customUpdateCell()`**），可参考`DemoFoldView.swift`中的**DemoFoldRow**。

### 全局设置

> **isOpen**：是否已经展开
>
> **foldHeight**：折叠高度（超过这个高度会折叠并展示打开按钮, 默认为100）

### 左侧自定义View

支持左侧自定义的View（如用户头像等），如果需要大规模复用（如动态列表等），建议使用继承并重写Cell中的`leftView`属性值来设置（可参考Demo中的 `DemoFoldView.swift` ）。

> **leftView**：左侧的自定义View (如果此属性, cell中原有的`leftView`将会隐藏)
>
> **leftViewSize**：左侧自定义View的大小（要正确显示leftView，就需要设置正确的值，不论是否重写）
>
> **spaceBetweenLeftAndContent**：左侧自定义View与右侧折叠View的间距

### 折叠区域内容

> **foldContentView**：设置可折叠区域内容控件 (如果此属性, cell中原有的`foldContentView`将会隐藏)

### 展开/收起按钮

> **defaultFoldButtonSetting**：默认的展开按钮相关样式属性（仅适用于默认的按钮，如果已重写了cell的`foldOpenView`或设置了row的foldOpenView，则此属性失效，除非自己再设置），包括 按钮文字、展开文字、文字颜色、文字对齐方式等
>
> **foldOpenView**：自定义 展开/收起 控件 (如果此属性, cell中原有的`foldOpenView`将会隐藏)
>
> **openViewPosition**：展开按钮位置
>
> ​		FoldOpenPosition类型，包括`bottom`(居于折叠区域下方)和`cover`（与折叠区域底部对齐并覆盖住一部分内容，展开时居于折叠区域下方）

## Cell

**FoldCell / FoldCollectionCell** 为可继承的单元格Cell类型，可继承并重写`leftView`（左侧view）、`foldContentView`（可折叠区域）、`foldOpenView`（展开、收起控件），包含如下属性：

> **leftView**：可重写，左侧view样式
>
> **foldContentView**：可重写，展开/折叠 内容区域控件，必须为`FoldContentView`及其子类
>
> **foldOpenView**：可重写，展开/折叠 按钮样式控件，必须为`BaseFoldOpenView`及其子类

### FoldContentView

**FoldContentView** 为可继承的 展开/收起 内容基类，子类中需实现`height(with width:)`方法来计算并返回内容的总高度

### BaseFoldOpenView

**BaseFoldOpenView** 为可继承的 展开/收起 控件基类，遵循 `FoldOpenViewType` 协议，且禁用交互。

## 使用举例

1、自定义折叠\展开内容View，或对应的Cell及Row，参考`DemoFoldView.swift`的内容，其中定义好了`DemoFoldRow`和`DemoFoldView`

2、添加到section中：

添加继承的子类Row（`DemoFoldRow`）:

```
TableSection("DemoFoldRow")
    <<< DemoFoldRow() { row in
        row.text = "文字内容"
        row.images = imageUrls // 图片地址字符串
        row.foldHeight = 120   // 折叠高度
        if count > 5 {
            // 大于5张图片展示左侧头像
            row.userImageUrl = ImageUrlsHelper.getRandomImage()
        }
    }
```

也可以直接添加FoldRow 或 FoldItem，然后通过row的属性设置来自定义(如果需要大量复用，还是建议继承row)：

```
TableSection("FoldRow")
		<<< FoldRow() { row in
            let foldContent = DemoFoldView()
            foldContent.text = "文字内容"
            foldContent.images = imageUrls 			// 图片地址字符串
            row.foldContentView = foldContent 	// 设置折叠区域
            row.foldHeight = 120 								// 折叠高度
            row.foldOpenView = DemoFoldButton() // 自定义的折叠按钮
            row.openViewPosition = .cover 			// 折叠按钮位置
            if count > 5 {
            		// 大于5张图片展示左侧头像
                let imageView = UIImageView()
                imageView.layer.cornerRadius = 15
                row.leftViewSize = CGSize(width: 30, height: 30)
                imageView.kf.setImage(with: URL(string: imgUrls.last!))
                row.leftView = imageView
            }
        }
    }
```

FoldItem的使用与FoldRow基本相同, 目前不建议将FoldItem添加到横向滚动的CollectionView中
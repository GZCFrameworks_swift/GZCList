# 如何自定义Row / Item

每个Row/Item都由两个部分组成：

> **Cell**：界面布局，供Tableview/Collectionview展示的Cell，单元格固定的界面展示控件在Cell中定义并添加并做好布局约束
>
> **Row/Item**：单元格数据对象，用于存储单元格的各种状态，由于Cell的复用机制，对同一cell的各种界面样式修改可以记录在属性中并在`customUpdateCell()`方法中完成修改。

为方便Row的使用，定义了**RowType**协议，包含了默认的初始化方法（带初始化完成回调），以及各种事件回调，回调的参数则会自动设置为对应的Row的类型，减少类型转换的麻烦，需使用final修饰的类来实现。

下面定义一个带UISwitch控件的Row/Item来简单说明如何自定义Row或Item：

### Row

1、定义Cell：

> **Cell**: 继承 **TableCellOf<T: Equatable>**，T代表了对应Row的value的类型，在`setup()`中完成布局方法，在`update()`中将value转换为控件展示值（也可在row的`customUpdateCell()`方法中完成），如：
>
> ```Swift
> class SwitchCell: TableCellOf<Bool> {
>     let valueSwitch: UISwitch = UISwitch()
>     
>     open override func setup() {
>         super.setup()
>         selectionStyle = .none
>         contentView.addSubview(valueSwitch)
>         valueSwitch.addTarget(self, action: #selector(switchDidChange), for: .touchUpInside)
>         
>         valueSwitch.snp.makeConstraints { (make) in
>             make.right.centerY.equalToSuperview()
>         }
>     }
>     
>     open override func update() {
>         super.update()
>         guard let v = value else {
>             return
>         }
>         valueSwitch.isOn = v
>     }
> }
> ```

2、定义Row

> **Row**：继承 **TableRowOf< Cell >**，Cell代表了Row对应的Cell，可在`customUpdateCell()`方法中调整cell的布局、设置对应的界面展示，重写`identifier`返回复用的identifier（如果没重写则表示不复用），如：
>
> ```Swift
> class _SwitchRow: TableRowOf<SwitchCell> {
>     // switch
>     /// 未选中背景色
>     public var switchTintColor: UIColor?
>     /// 选中背景色
>     public var switchOnTintColor: UIColor?
>     /// 滑块颜色
>     public var switchSliderColor: UIColor?
>     
>     // 更新cell的布局
>     open override func customUpdateCell() {
>         super.customUpdateCell()
>         guard let cell = cell else {
>             return
>         }
>         
>         cell.valueSwitch.thumbTintColor = switchSliderColor
>         cell.valueSwitch.onTintColor = switchOnTintColor
>         cell.valueSwitch.backgroundColor = switchTintColor
> 
>         cell.contentView.snp.updateConstraints { (make) in
>             make.edges.equalTo(contentInsets)
>         }
>     }
>     
>     open override var identifier: String {
>         return "_SwitchRow"
>     }
> }
> ```

3、添加**RowType**协议实现类（无需实现内容任何，该做的在_SwitchRow中都做了）：

```
final class SwitchRow: _SwitchRow, RowType {
}
```

最终使用的Row就是实现了`RowType`协议的类**SwitchRow**。

### Item

定义Item与定义Row的方式基本一致，修改父类以及增加尺寸计算的方法实现即可：

1、定义Cell：

> **Cell**: 继承 **CollectionCellOf<T: Equatable>**，T代表了对应Row的value的类型，在`setup()`中完成布局方法，在`update()`中将value转换为控件展示值（也可在row的`customUpdateCell()`方法中完成），如：
>
> ```Swift
> class CollectionSwitchCell: CollectionCellOf<Bool> {
>     let valueSwitch: UISwitch = UISwitch()
>     
>     open override func setup() {
>         super.setup()
>         contentView.addSubview(valueSwitch)
>         valueSwitch.addTarget(self, action: #selector(switchDidChange), for: .touchUpInside)
>         
>         valueSwitch.snp.makeConstraints { (make) in
>             make.right.centerY.equalToSuperview()
>         }
>     }
>     
>     open override func update() {
>         super.update()
>         guard let v = value else {
>             return
>         }
>         valueSwitch.isOn = v
>     }
> }
> ```

2、定义Row

> **Item**：继承 **CollectionItemOf< Cell >**，Cell代表了Item对应的Cell，同样的，可在`customUpdateCell()`方法中调整cell的布局、设置对应的界面展示，需要注意的是，Item中**必须**重写`identifier`返回复用的identifier，同时需要添加单元格**尺寸**的对应计算方法，并返回对应的结果，如：
>
> ```Swift
> class _SwitchRow: CollectionItemOf<CollectionSwitchCell> {
>     /// 固定高度
>     public var aspectHeight: CGFloat = 44
>   
>     // switch
>     /// 未选中背景色
>     public var switchTintColor: UIColor?
>     /// 选中背景色
>     public var switchOnTintColor: UIColor?
>     /// 滑块颜色
>     public var switchSliderColor: UIColor?
>     
>     // 更新cell的布局
>     open override func customUpdateCell() {
>         super.customUpdateCell()
>         guard let cell = cell else {
>             return
>         }
>         
>         cell.valueSwitch.thumbTintColor = switchSliderColor
>         cell.valueSwitch.onTintColor = switchOnTintColor
>         cell.valueSwitch.backgroundColor = switchTintColor
> 
>         cell.contentView.snp.updateConstraints { (make) in
>             make.edges.equalTo(contentInsets)
>         }
>     }
>     
>     open override var identifier: String {
>         return "_SwitchItem"
>     }
>   
>  	 	// 计算宽高
>    	// 如果不重写方法，则默认返回aspectRatio指定的比例尺寸，如果aspectRatio未指定，则默认返回1:1尺寸的单元格
>   	/// 根据宽度计算高度
>     open override func cellHeight(for width: CGFloat) -> CGFloat {
>         if let height = aspectHeight(width) {
>             return height
>         }
>         return aspectHeight
>     }
>     
>   	/// 根据高度计算宽度
>     open override func cellWidth(for height: CGFloat) -> CGFloat {
>         if let width = aspectWidth(height) {
>             return width
>         }
>         return aspectHeight
>     }
> }
> ```

3、添加**RowType**协议实现类（无需实现内容任何，该做的在_SwitchItem中都做了）：

```swift
final class SwitchItem: _SwitchItem, RowType {
}
```

最终使用的Row就是实现了`RowType`协议的类**SwitchItem**。
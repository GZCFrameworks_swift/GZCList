//
//  DemoViewController.swift
//  GZCList_Example
//
//  Created by Guo ZhongCheng on 2020/9/22.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCList

class DemoViewController: UIViewController {
    
    lazy var tableView = FormTableView()
    
    lazy var collectionView = FormCollectionView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        setUpTableRows()
        setUpCollectionItems()
    }
    
    func setUpViews() {
        collectionView.backgroundColor = .white
        
        view.addSubview(tableView)
        view.addSubview(collectionView)
        
        tableView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
        collectionView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
            make.height.equalTo(tableView.snp.height)
            make.top.equalTo(tableView.snp.bottom)
        }
    }
    
    func setUpTableRows() {
        tableView.form +++ TableSection("HtmlInfoRow")
            <<< HtmlInfoRow() { row in
                row.value = "HtmlInfoRow是用于展示Html代码字符串的Row，设置value为Html代码，即可展示\n展示出来后会自动调整高度，设置estimatedSize表示预估的size，会根据size的比例预先设置大小\n设置contentInsets可调整内容的四边间距"
                row.contentInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                row.estimatedSize = CGSize(width: 100, height: 30)
            }
            <<< HtmlInfoRow() { row in
                row.value = "<img src = \"http://img.alicdn.com/imgextra/i3/124158638/O1CN01AlLzW02DgFnqvcWtB_!!124158638.jpg\"/>"
                row.estimatedSize = CGSize(width: 750, height: 730)
                row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            }
            <<< HtmlInfoRow() { row in
                row.value = "<img src = \"http://img.alicdn.com/imgextra/i3/124158638/O1CN01UDk2nT2DgFntE75Mg_!!124158638.jpg\"/>"
                row.estimatedSize = CGSize(width: 750, height: 730)
                row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            }
            <<< HtmlInfoRow() { row in
                row.value = "<img src = \"http://img.alicdn.com/imgextra/i2/124158638/O1CN019BHZod2DgFnslzXhR_!!124158638.jpg\"/>"
                row.estimatedSize = CGSize(width: 750, height: 730)
                row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            }
            <<< HtmlInfoRow() { row in
                row.value = "<img src = \"http://img.alicdn.com/imgextra/i2/124158638/O1CN01EPcjWn2DgFnqwc1kG_!!124158638.jpg\"/>"
                row.estimatedSize = CGSize(width: 750, height: 730)
                row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            }
            <<< HtmlInfoRow() { row in
                row.value = "<img src = \"http://img.alicdn.com/imgextra/i1/124158638/O1CN019pgtii2DgFnwgeFxu_!!124158638.jpg\"/>"
                row.estimatedSize = CGSize(width: 750, height: 730)
                row.contentInsets = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
            }
    }
    
    func setUpCollectionItems() {
        let multivalusedSection = CollectionMultivalusedSection(multivaluedOptions: [.Reorder, .Delete], header: "item可编辑的Section1", footer: "可编辑Section1结束", { (section) in
            section.header?.shouldSuspension = true
            section.contentInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        })
        for i in 0 ... 10 {
            multivalusedSection <<< CollectionInlineRootItem() { row in
                let value = DemoItem(imageUrl: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1600664112316&di=d6299c89a2d522ddc53361dfc1ec6c4d&imgtype=0&src=http%3A%2F%2Fa4.att.hudong.com%2F27%2F67%2F01300000921826141299672233506.jpg", title: "标题\(i)")
                row.value = value
                row.inlineRowOpenBlock = { r in
                    let value = DemoItem(imageUrl: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1600428814333&di=913f844659946d7fb7d6ed1f1f67a72e&imgtype=0&src=http%3A%2F%2Fa1.att.hudong.com%2F05%2F00%2F01300000194285122188000535877.jpg", title: "打开的\(i)")
                    r.value = value
                }
            }
        }
        collectionView.form +++ multivalusedSection
    }
}


//
//  DemoInlineRow.swift
//  GZCList_Example
//
//  Created by Guo ZhongCheng on 2020/9/8.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCList

struct InlineRowValue {
    var title: String
}

class InlineRootRow: TableRowOf<LabelCell>, InlineTableRowType {
    typealias InlineRow = InlineOpenRow
    
    func setupInlineRow(_ inlineRow: InlineOpenRow) {
        inlineRow.value = "打开了"
        inlineRow.cellHeight = UITableView.automaticDimension
    }
    
    override func customDidSelect() {
        super.customDidSelect()
        if !isDisabled {
            toggleInlineRow()
        }
    }
}

class InlineOpenRow: TableRowOf<LabelCell> {
    
}

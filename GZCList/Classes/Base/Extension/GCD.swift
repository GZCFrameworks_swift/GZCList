//
//  GCD.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/7/16.
//

import Foundation

/// 在主线程执行代码块
/// - Parameters:
///   - isAsync: 是否异步执行，默认为`true`
///   - execute: 在主线程执行的代码块
public func mainThread(isAsync: Bool = true, main execute: @escaping () -> Void) {
    if Thread.isMainThread {
        execute()
    } else if isAsync {
        DispatchQueue.main.async(execute: execute)
    } else {
        DispatchQueue.main.sync(execute: execute)
    }
}

//
//  TextField+Limit.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/7/16.
//

import Foundation

/// 中英文混编长度限制
public extension UITextField {
    // 限制字符输入,按英文，数字来判断，中文算2个,limitCount:字符总数按英文个数限制
    func limitTextCount(_ limitCount: Int) -> Int {
        // 总的数量
        let allCount = self.text?.count ?? 0
        // 高亮的数量
        var markedCount = 0
        if let markedRange = self.markedTextRange, let markedText = self.text(in: markedRange) {
            markedCount = markedText.count
        }
        // 剩下在显示的文本数量
        let remainCount = allCount - markedCount
        let remainText = String(self.text?.prefix(remainCount) ?? "")
        
        /// 计算总数
        let  (totalCount, showText) = remainText.textCount(limitCount)
        if totalCount > limitCount {
            self.text = showText
        }
        return totalCount
    }
}

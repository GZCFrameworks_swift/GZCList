//
//  FormCollectionView+Refresh.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/10/21.
//

import ESPullToRefresh

public extension UICollectionView {
    /// 这里需要等待刷新动画完成后等待添加Item的动画结束再隐藏，避免跳动
    override func stopLoadingMore() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            super.stopLoadingMore()
        }
    }
    
    override func noticeNoMoreData() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            super.noticeNoMoreData()
        }
    }
}

public extension FormCollectionView {
    override func addHeaderRefresher(_ handler: @escaping ESRefreshHandler) {
        alwaysBounceVertical = true
        super.addHeaderRefresher(handler)
    }
}

public extension FormCollectionViewController {
    // MARK: 下拉刷新
    /// 添加下拉刷新
    func addHeaderRefresher(_ handler: @escaping ESRefreshHandler) {
        collectionView.alwaysBounceVertical = true
        collectionView.addHeaderRefresher(handler)
    }
    /// 添加自定义刷新头
    func addHeaderRefresher(animator: ESRefreshProtocol & ESRefreshAnimatorProtocol, handler: @escaping ESRefreshHandler) {
        collectionView.alwaysBounceVertical = true
        collectionView.addHeaderRefresher(animator: animator, handler: handler)
    }
    
    // MARK: 上拉加载
    /// 添加上拉加载
    func addFooterRefresher(_ handler: @escaping ESRefreshHandler) {
        collectionView.addFooterRefresher(handler)
    }
    /// 添加自定义上拉加载
    func addFooterRefresher( animator: ESRefreshProtocol & ESRefreshAnimatorProtocol, handler: @escaping ESRefreshHandler) {
        collectionView.addFooterRefresher(animator: animator, handler: handler)
    }
    
    // MARK: 移除
    /// 移除下拉刷新
    func removeRefreshHeader() {
        collectionView.removeRefreshHeader()
    }
    
    /// 移除上拉加载
    func removeRefreshFooter() {
        collectionView.removeRefreshFooter()
    }
    
    // MARK: 停止加载
    /// 开始下拉刷新
    func startPullToRefresh() {
        collectionView.startPullToRefresh()
    }
    
    /// 自动下拉刷新
    func autoPullToRefresh() {
        collectionView.autoPullToRefresh()
    }
    
    /// 停止下拉刷新
    /// - Parameters:
    ///   - ignoreDate: 是否不记录此次下拉的时间
    ///   - ignoreFooter: 是否隐藏加载更多
    func stopPullToRefresh(ignoreDate: Bool = false, ignoreFooter: Bool = false) {
        collectionView.stopPullToRefresh(ignoreDate: ignoreDate, ignoreFooter: ignoreFooter)
    }
    
    /// 显示没有更多
    func noticeNoMoreData() {
        collectionView.noticeNoMoreData()
    }
    
    /// 显示加载更多
    func resetNoMoreData() {
        collectionView.resetNoMoreData()
    }
    
    /// 停止上拉加载
    func stopLoadingMore() {
        collectionView.stopLoadingMore()
    }
    
    /// 停止头部与尾部的刷新
    func stopRefreshing() {
        collectionView.stopRefreshing()
    }
}

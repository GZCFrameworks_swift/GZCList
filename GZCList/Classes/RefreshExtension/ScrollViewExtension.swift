//
//  ScrollViewExtension.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/10/21.
//

import ESPullToRefresh

public extension UIScrollView {
    // MARK: 下拉刷新
    /// 添加下拉刷新
    @objc func addHeaderRefresher(_ handler: @escaping ESRefreshHandler) {
        if let header = ScrollRefreshConfig.shared.headerAnimator?() {
            es.addPullToRefresh(animator: header, handler: handler)
        } else {
            es.addPullToRefresh(handler: handler)
        }
    }
    /// 添加自定义刷新头
    func addHeaderRefresher(animator: ESRefreshProtocol & ESRefreshAnimatorProtocol, handler: @escaping ESRefreshHandler) {
        es.addPullToRefresh(animator: animator, handler: handler)
    }
    
    // MARK: 上拉加载
    /// 添加上拉加载
    func addFooterRefresher(_ handler: @escaping ESRefreshHandler) {
        if let footer = ScrollRefreshConfig.shared.footerAnimator?() {
            es.addInfiniteScrolling(animator: footer, handler: handler)
        } else {
            es.addInfiniteScrolling(handler: handler)
        }
    }
    /// 添加自定义上拉加载
    func addFooterRefresher( animator: ESRefreshProtocol & ESRefreshAnimatorProtocol, handler: @escaping ESRefreshHandler) {
        es.addInfiniteScrolling(animator: animator, handler: handler)
    }
    
    // MARK: 移除
    /// 移除下拉刷新
    func removeRefreshHeader() {
        self.es.removeRefreshHeader()
    }
    
    /// 移除上拉加载
    func removeRefreshFooter() {
        self.es.removeRefreshFooter()
    }
    
    // MARK: 停止加载
    /// 开始下拉刷新
    func startPullToRefresh() {
        self.es.startPullToRefresh()
    }
    
    /// 自动下拉刷新
    func autoPullToRefresh() {
        self.es.autoPullToRefresh()
    }
    
    /// 停止下拉刷新
    /// - Parameters:
    ///   - ignoreDate: 是否不记录此次下拉的时间
    ///   - ignoreFooter: 是否隐藏加载更多
    func stopPullToRefresh(ignoreDate: Bool = false, ignoreFooter: Bool = false) {
        /// 延迟0.3秒 等待上一次的收起动画结束，可以避免跳动
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            if self.header?.isRefreshing ?? false {
                self.es.stopPullToRefresh(ignoreDate: ignoreDate, ignoreFooter: ignoreFooter)
            }
        }
    }
    
    /// 显示没有更多
    @objc func noticeNoMoreData() {
        /// 延迟0.3秒 等待刷新动画结束，可以减少跳动
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            if self.footer?.isRefreshing ?? false {
                self.es.noticeNoMoreData()
            }
        }
    }
    
    /// 显示加载更多
    func resetNoMoreData() {
        self.es.resetNoMoreData()
    }
    
    /// 停止上拉加载
    @objc func stopLoadingMore() {
        /// 延迟0.3秒 等待刷新动画结束，可以减少跳动
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            if self.footer?.isRefreshing ?? false {
                self.es.stopLoadingMore()
            }
        }
    }
    
    /// 停止头部与尾部的刷新
    func stopRefreshing() {
        self.stopPullToRefresh()
        self.stopLoadingMore()
    }
}

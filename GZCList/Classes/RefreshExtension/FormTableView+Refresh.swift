//
//  FormTableView+Refresh.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/10/21.
//

import ESPullToRefresh

public extension FormTableViewController {
    // MARK: 下拉刷新
    /// 添加下拉刷新
    func addHeaderRefresher(_ handler: @escaping ESRefreshHandler) {
        tableView.addHeaderRefresher(handler)
    }
    /// 添加自定义刷新头
    func addHeaderRefresher(animator: ESRefreshProtocol & ESRefreshAnimatorProtocol, handler: @escaping ESRefreshHandler) {
        tableView.addHeaderRefresher(animator: animator, handler: handler)
    }
    
    // MARK: 上拉加载
    /// 添加上拉加载
    func addFooterRefresher(_ handler: @escaping ESRefreshHandler) {
        tableView.addFooterRefresher(handler)
    }
    /// 添加自定义上拉加载
    func addFooterRefresher( animator: ESRefreshProtocol & ESRefreshAnimatorProtocol, handler: @escaping ESRefreshHandler) {
        tableView.addFooterRefresher(animator: animator, handler: handler)
    }
    
    // MARK: 移除
    /// 移除下拉刷新
    func removeRefreshHeader() {
        tableView.removeRefreshHeader()
    }
    
    /// 移除上拉加载
    func removeRefreshFooter() {
        tableView.removeRefreshFooter()
    }
    
    // MARK: 停止加载
    /// 开始下拉刷新
    func startPullToRefresh() {
        tableView.startPullToRefresh()
    }
    
    /// 自动下拉刷新
    func autoPullToRefresh() {
        tableView.autoPullToRefresh()
    }
    
    /// 停止下拉刷新
    /// - Parameters:
    ///   - ignoreDate: 是否不记录此次下拉的时间
    ///   - ignoreFooter: 是否隐藏加载更多
    func stopPullToRefresh(ignoreDate: Bool = false, ignoreFooter: Bool = false) {
        tableView.stopPullToRefresh(ignoreDate: ignoreDate, ignoreFooter: ignoreFooter)
    }
    
    /// 显示没有更多
    func noticeNoMoreData() {
        tableView.noticeNoMoreData()
    }
    
    /// 显示加载更多
    func resetNoMoreData() {
        tableView.resetNoMoreData()
    }
    
    /// 停止上拉加载
    func stopLoadingMore() {
        tableView.stopLoadingMore()
    }
    
    /// 停止头部与尾部的刷新
    func stopRefreshing() {
        tableView.stopRefreshing()
    }
}

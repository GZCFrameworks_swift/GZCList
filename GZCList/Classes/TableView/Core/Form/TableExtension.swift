//
//  TableExtension.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/9/7.
//

import UIKit

extension UIView {

    public func formCell() -> TableCell? {
        if self is UITableViewCell {
            return self as? TableCell
        }
        return superview?.formCell()
    }
    
}

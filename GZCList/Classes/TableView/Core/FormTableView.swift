//
//  FormTableView.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/9/5.
//

import UIKit

open class FormTableView: UITableView, TableViewHandlerAnimationDelegate {
    
    // 处理tableview代理相关方法的类
    public lazy var handler: TableViewHandler = TableViewHandler()
    // handler代理, 包括cell的value改变回调以及scrollviewDelegate相关方法
    public weak var handerDelegate: TableViewHandlerDelegate? {
        didSet {
            handler.delegate = handerDelegate
        }
    }
    
    public var form: TableForm {
        return handler.form
    }
    
    // MARK:- 初始化方法
    public convenience init() {
        self.init(frame: .zero, style: .plain)
    }
    
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        defaultSettings()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        defaultSettings()
    }
    
    func defaultSettings(){
        handler.tableView = self
        handler.animationDelegate = self
        cancelAdjustsScrollView()
    }
    
    /// 去除顶部留白
    public func cancelAdjustsScrollView() {
        if #available(iOS 11.0, *) {
            contentInsetAdjustmentBehavior = .never
        }
    }
    
    // MARK:- TableViewHandlerAnimationDelegate
    open func insertAnimation(forRows rows: [TableRow]) -> UITableView.RowAnimation {
        .automatic
    }
    
    open func deleteAnimation(forRows rows: [TableRow]) -> UITableView.RowAnimation {
        .automatic
    }
    
    open func reloadAnimation(oldRows: [TableRow], newRows: [TableRow]) -> UITableView.RowAnimation {
        .automatic
    }
    
    open func insertAnimation(forSections sections: [TableSection]) -> UITableView.RowAnimation {
        .automatic
    }
    
    open func deleteAnimation(forSections sections: [TableSection]) -> UITableView.RowAnimation {
        .automatic
    }
    
    open func reloadAnimation(oldSections: [TableSection], newSections: [TableSection]) -> UITableView.RowAnimation {
        .automatic
    }
}

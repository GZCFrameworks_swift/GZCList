//
//  FormTableViewController.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/6/2.
//

import UIKit

open class FormTableViewController: UIViewController, TableViewHandlerAnimationDelegate {
    // tableView
    @IBOutlet public var tableView: UITableView!
    // tableView的样式，在didLoad之前设置
    private var tableViewStyle: UITableView.Style = .plain
    
    
    // tableView代理处理类
    public var handler: TableViewHandler = TableViewHandler()
    // handler代理, 包括cell的value改变回调以及scrollviewDelegate相关方法
    public weak var handerDelegate: TableViewHandlerDelegate? {
        didSet {
            handler.delegate = handerDelegate
        }
    }
    
    public var form: TableForm {
        return handler.form
    }
    
    public init(style: UITableView.Style) {
        super.init(nibName: nil, bundle: nil)
        tableViewStyle = style
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        if tableView == nil {
            tableView = UITableView(frame: view.bounds, style: tableViewStyle)
            tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            tableView.cellLayoutMarginsFollowReadableWidth = false
        }
        if tableView.superview == nil {
            view.addSubview(tableView)
        }
        handler.tableView = self.tableView
        handler.animationDelegate = self
        cancelAdjustsScrollView()
    }
    
    /// 去除顶部留白
    public func cancelAdjustsScrollView() {
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    // MARK:- TableViewHandlerAnimationDelegate
    open func insertAnimation(forRows rows: [TableRow]) -> UITableView.RowAnimation {
       .automatic
    }

    open func deleteAnimation(forRows rows: [TableRow]) -> UITableView.RowAnimation {
       .automatic
    }

    open func reloadAnimation(oldRows: [TableRow], newRows: [TableRow]) -> UITableView.RowAnimation {
       .automatic
    }

    open func insertAnimation(forSections sections: [TableSection]) -> UITableView.RowAnimation {
       .automatic
    }

    open func deleteAnimation(forSections sections: [TableSection]) -> UITableView.RowAnimation {
       .automatic
    }

    open func reloadAnimation(oldSections: [TableSection], newSections: [TableSection]) -> UITableView.RowAnimation {
       .automatic
    }
}

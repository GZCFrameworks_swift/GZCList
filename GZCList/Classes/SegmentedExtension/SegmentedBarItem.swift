//
//  SegmentedBarItem.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/4/23.
//

// MARK:- SegmentedBarCollectionCell
/// 分段栏的cell
open class SegmentedBarCollectionCell: CollectionCellOf<Bool> {
    
    let segmentView: JXSegmentedView = JXSegmentedView()
    
    open override func setup() {
        super.setup()
        
        contentView.addSubview(segmentView)
        segmentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

// MARK:- SegmentedBarRow
/// 分段栏的Row
public final class SegmentedBarItem: CollectionItemOf<SegmentedBarCollectionCell> {
    /// 复用id (复用可能会导致指示条出不来，因此默认不复用（使用index来生成不同的identifier）)
    public override var identifier: String {
        if let outside = outsideIdentifier {
            return outside
        }
        guard let index = indexPath else {
            return "SegmentedBarItem"
        }
        return "SegmentedBarItem_\(index.section)_\(index.row)"
    }
    /// 外部传入的identifier
    var outsideIdentifier: String?
    
    /// 当前位置
    var currentIndex: Int = 0
    
    /// bar的数据源
    var segmentedViewDataSource: JXSegmentedViewDataSource!
    
    /// 指示器
    public var indicators = [JXSegmentedIndicatorProtocol & UIView]()
    
    /// bar点击切换页面时是否带滚动动画
    public var isContentScrollAnimationEnabled: Bool = true
    
    /// segmentedView的代理，用于返回对应的view和是否可以选中
    public weak var delegate: JXSegmentedViewDelegate?
    /// 内容视图
    public var listContainer: JXSegmentedViewListContainer? = nil {
        didSet {
            cell?.segmentView.listContainer = listContainer
        }
    }
    
    public override func customUpdateCell() {
        super.customUpdateCell()
        guard let cell = cell else {
            return
        }
        
        cell.segmentView.backgroundColor = backgroundColor
        cell.segmentView.contentEdgeInsetLeft = contentInsets.left
        cell.segmentView.contentEdgeInsetRight = contentInsets.right
        cell.segmentView.isContentScrollViewClickTransitionAnimationEnabled = isContentScrollAnimationEnabled
        cell.segmentView.dataSource = segmentedViewDataSource
        cell.segmentView.delegate = delegate
        cell.segmentView.indicators = indicators
        cell.segmentView.listContainer = listContainer
        cell.segmentView.defaultSelectedIndex = currentIndex
        cell.segmentView.reloadData()
    }
    
    public override func didEndDisplay() {
        guard let cell = cell else {
            return
        }
        currentIndex = cell.segmentView.selectedIndex
        super.didEndDisplay()
    }
    
    var itemSize: CGFloat = 0
    
    /// 固定高度与source创建
    /// - Parameter heightOrWidth: 高度或宽度
    /// - Parameter source: JXSegmentedTitleDataSource，用于指定分段内容
    public init(tag: String? = nil, heightOrWidth: CGFloat, dataSource: JXSegmentedViewDataSource, identifier: String? = nil, _ initializer: (SegmentedBarItem) -> Void = { _ in }) {
        super.init(title: nil, tag: nil)
        itemSize = heightOrWidth
        segmentedViewDataSource = dataSource
        outsideIdentifier = identifier
        initializer(self)
    }
    
    required init(title: String? = nil, tag: String? = nil) {
        super.init(title: title, tag: tag)
    }
    
    public override func cellWidth(for height: CGFloat) -> CGFloat {
        return itemSize
    }
    
    public override func cellHeight(for width: CGFloat) -> CGFloat {
        return itemSize
    }
}

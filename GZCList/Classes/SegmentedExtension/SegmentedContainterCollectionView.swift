//
//  SegmentedContainterCollectionView.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/4/23.
//

@_exported import JXSegmentedView

class SegmentedContainterCollectionView: FormCollectionView, JXSegmentedViewListContainer{
    
    /// bar点击切换页面时是否带滚动动画
    var isAnimationWhenClickItem: Bool = true
    
    var defaultSelectedIndex: Int = 0
    
    func contentScrollView() -> UIScrollView {
        return self
    }
    
    func didClickSelectedItem(at index: Int) {
    }
}

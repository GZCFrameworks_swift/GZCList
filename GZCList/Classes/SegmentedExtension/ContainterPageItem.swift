//
//  ContainterPageItem.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/4/23.
//


public enum GZCPageContentType: Equatable {
    case view(_ view: UIView)
    case controller(_ vc: UIViewController)
}

public class ContainterPageCell: CollectionCellOf<GZCPageContentType> {
    override public func update() {
        for view in contentView.subviews {
            view.removeFromSuperview()
        }
        guard let type = value else {
            return
        }
        
        switch type {
        case .view(let view):
            view.removeFromSuperview()
            
            contentView.addSubview(view)
            view.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        case .controller(let vc):
            vc.view.removeFromSuperview()
            
            contentView.addSubview(vc.view)
            vc.view.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
        
        layoutIfNeeded()
    }
}

final public class ContainterPageItem: CollectionItemOf<ContainterPageCell>, RowType {
    public override var identifier: String {
        switch value {
        case .view:
            return "ViewContainterPageItem"
        case .controller:
            return "ControllerContainterPageItem"
        case .none:
            return "ContainterPageItem"
        }
    }
    
    public override func willDisplay() {
        super.willDisplay()
        switch value {
        case .controller(let vc):
            vc.beginAppearanceTransition(true, animated: false)
        default: break
        }
    }
    
    public override func didEndDisplay() {
        super.didEndDisplay()
        switch value {
        case .controller(let vc):
            vc.beginAppearanceTransition(false, animated: false)
        default: break
        }
    }
    
    public override func cellWidth(for height: CGFloat) -> CGFloat {
        guard let delegate = form?.delegate as? CollectionViewHandler,
              let collectionView = delegate.collectionView else {
            return 0
        }
        return collectionView.bounds.width
    }
    
    public override func cellHeight(for width: CGFloat) -> CGFloat {
        guard let delegate = form?.delegate as? CollectionViewHandler,
              let collectionView = delegate.collectionView else {
            return 0
        }
        return collectionView.bounds.height
    }
}

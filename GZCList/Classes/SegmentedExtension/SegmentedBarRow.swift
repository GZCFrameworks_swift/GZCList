//
//  SegmentedBarRow.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2021/4/23.
//

// MARK:- SegmentedBarCell
/// 分段栏的cell
open class SegmentedBarCell: TableCellOf<Bool> {
    
    let segmentView: JXSegmentedView = JXSegmentedView()
    
    open override func setup() {
        super.setup()
        
        contentView.addSubview(segmentView)
        segmentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

// MARK:- SegmentedBarRow
/// 分段栏的Row
public final class SegmentedBarRow: TableRowOf<SegmentedBarCell> {
    /// 复用id (复用可能会导致指示条出不来，因此默认不复用（使用index来生成不同的identifier）)
    public override var identifier: String? {
        if let outside = outsideIdentifier {
            return outside
        }
        guard let index = indexPath else {
            return nil
        }
        return "SegmentedBarItem_\(index.section)_\(index.row)"
    }
    /// 外部传入的identifier
    var outsideIdentifier: String?
    
    /// 当前位置
    var currentIndex: Int = 0
    
    /// bar的数据源
    public var segmentedViewDataSource: JXSegmentedViewDataSource! {
        didSet {
            updateCell()
        }
    }
    
    /// 指示器
    public var indicators = [JXSegmentedIndicatorProtocol & UIView]()
    
    /// bar点击切换页面时是否带滚动动画
    public var isContentScrollAnimationEnabled: Bool = true
    
    /// segmentedView的代理，用于返回对应的view和是否可以选中
    public weak var delegate: JXSegmentedViewDelegate?
    /// 内容视图
    public var listContainer: JXSegmentedViewListContainer? = nil {
        didSet {
            cell?.segmentView.listContainer = listContainer
        }
    }
    
    public override func customUpdateCell() {
        super.customUpdateCell()
        guard let cell = cell else {
            return
        }
        
        cell.segmentView.backgroundColor = backgroundColor
        cell.segmentView.contentEdgeInsetLeft = contentInsets.left
        cell.segmentView.contentEdgeInsetRight = contentInsets.right
        cell.segmentView.isContentScrollViewClickTransitionAnimationEnabled = isContentScrollAnimationEnabled
        cell.segmentView.dataSource = segmentedViewDataSource
        cell.segmentView.delegate = delegate
        cell.segmentView.indicators = indicators
        cell.segmentView.listContainer = listContainer
        cell.segmentView.defaultSelectedIndex = currentIndex
        cell.segmentView.reloadData()
    }
    
    public override func didEndDisplay() {
        guard let cell = cell else {
            return
        }
        currentIndex = cell.segmentView.selectedIndex
        super.didEndDisplay()
    }
    
    /// 固定高度与source创建
    /// - Parameter height: 高度
    /// - Parameter source: JXSegmentedTitleDataSource，用于指定分段内容
    public init(tag: String? = nil, height: CGFloat, dataSource: JXSegmentedViewDataSource, identifier: String? = nil, _ initializer: (SegmentedBarRow) -> Void = { _ in }) {
        super.init(title: nil, tag: nil)
        cellHeight = height
        segmentedViewDataSource = dataSource
        outsideIdentifier = identifier
        initializer(self)
    }
    
    required init(title: String? = nil, tag: String? = nil) {
        super.init(title: title, tag: tag)
    }
}

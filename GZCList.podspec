#
# Be sure to run `pod lib lint GZCList.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GZCList'
  s.version          = '0.4.4'
  s.summary          = 'Swift编写的快速创建TableView和CollectionView的框架'

  s.description      = <<-DESC
      使用Swift编写的快速创建ListView和CollectionView的框架，灵感来自于Eureka
                       DESC

  s.homepage         = 'https://gitlab.com/GZCFrameworks_swift/GZCList'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guo ZhongCheng' => 'gzhongcheng@qq.com' }
  s.source           = { :git => 'https://gitlab.com/GZCFrameworks_swift/GZCList.git', :tag => s.version.to_s }
  
  s.swift_version = "5.3"
  s.ios.deployment_target = '10.0'
  
  s.default_subspec = "Segmented"
  s.subspec "Base" do |ss|
    ss.source_files  = "GZCList/Classes/Base/**/*.swift"
    ss.dependency 'SnapKit'
    ss.dependency 'GZCExtends/WebImage'
  end
  
  s.subspec "CollectionView" do |ss|
    ss.source_files = "GZCList/Classes/CollectionView/**/*.swift"
    ss.dependency 'GZCList/Base'
  end
  
  s.subspec "TableView" do |ss|
    ss.source_files = "GZCList/Classes/TableView/**/*.swift"
    ss.dependency 'GZCList/Base'
  end
  
  s.subspec "Refresh" do |ss|
    ss.source_files = "GZCList/Classes/RefreshExtension/**/*.swift"
    ss.dependency 'GZCList/CollectionView'
    ss.dependency 'GZCList/TableView'
    ss.dependency 'ESPullToRefresh'
  end
  
  s.subspec "Segmented" do |ss|
    ss.source_files = "GZCList/Classes/SegmentedExtension/**/*.swift"
    ss.dependency 'GZCList/Refresh'
    ss.dependency 'JXSegmentedView'
  end
end

# GZCList

使用Swift编写的快速创建ListView和CollectionView的框架，灵感来自于[Eureka](https://github.com/xmartlabs/Eureka)，相比于Eureka，增加了以下功能：

1、支持TableView的Cell的复用

2、增加了一些运算符，略微修改了原有运算符的逻辑

3、增加了对CollectionView的支持：

- 快速创建系统样式Collection流（横向、纵向），`arrangement`设置为`.system`
- 快速创建瀑布流（横向、纵向），同时支持不同section设置不同的列数，`arrangement`设置为`.flow`
- 快速创建固定行高自动换行的布局方式（横向、纵向），`arrangement`设置为`.aline`
- 快速创建`.flow`和`.aline`布局混用的Collection流，`arrangement`设置为`.blend`，同时需指定各section对象的`arrangement`属性值（`.flow`或`.aline`，默认为`.flow`）
- 支持自定义Section的Header、Footer样式
- 支持Header、Footer的悬浮（`arrangement`为`.system`时不支持悬浮），可通过设置Header、Footer的`shouldSuspension`属性控制是否悬浮
- `CollectionMultivalusedSection`支持长按拖动交换item位置
- 支持自定义布局layout，将`arrangement`设置为`.custom(layout)`即可，`layout`值可设置为`UICollectionViewFlowLayout`及其子类对象

4、框架中的TableView和CollectionView都支持单独的View创建，而不必须使用Controller

5、增加上下拉刷新方法，依赖于ESPullToRefresh，可通过配置`ScrollRefreshConfig`对应属性设置统一样式。

## 要求

- Xcode 9.2+
- Swift 5+

### 示例程序

你可以clone这个项目，然后运行Example来查看GZCList的大部分特性。

## 安装

GZCList 支持 [CocoaPods](https://cocoapods.org). 安装
添加下面代码到项目的 Podfile 文件中:

```ruby
pod 'GZCList'
```

## 运算符相关

框架中定义了一些运算符，用于快速操作Frame和Section，具体如下：

>以下所有类型都包含其子类,
>
>以下`Form`在`TableView`中均表示为`TableForm`，在`CollectionView`中均表示为`CollectionForm`
>
>以下`Section`在`TableView`中均表示为`TableSection`，在`CollectionView`中均表示为`CollectionSection`
>
>以下`Row`在`TableView`中均表示为`TableRow`，在`CollectionView`中均表示为`CollectionItem`

| 运算符 | 描述                                                   | 左侧对象          | 右侧对象                                                     |
| ------ | ------------------------------------------------------ | ----------------- | ------------------------------------------------------------ |
| `+++`    | 添加Section或Row（添加Row时会自动添加一个Section）     | `Form`            | `Section`或`Row`                                             |
| `+++!`   | 添加Section或Row，并通知更新界面                       | `Form`          | `Section`或`Row`                                                      |
| `<<<`    | 添加Row                                                | `Section`         | `Row`                                                        |
| `<<<!`   | 添加Row或[Row]，并通知更新界面                                |  `Section`       | `Row`或`[Row]`                                              |
| +=     | 添加右侧对象数组中的所有元素                           | `Form`或`Section` | `[Section]`或`[Row]`                                         |
| `>>>`   | 替换右侧对象数组中的所有元素到指定位置，并通知更新界面 | `Form`或`Section` | `>>>` 有两种使用方式：<br />1、使用 `>>> [Section]`或`[Row]`，将目标数组的元素直接替换原有的`Form`或`Section`中的所有元素<br />2、使用 `>>> (n ..< m, [Section]或[Row])`，将替换目标数组到指定范围（如，n ..< m 表示 n到m-1的所有元素） |
| `---`    | 移除所有元素，并通知更新界面                           | `Form`或`Section` | 无                                                           |

## 创建列表

### 创建TableView

1、直接使用`FormTableView`创建，如：

```swift
import GZCList

let tableView = FormTableView()

tableView.form +++ TableSection("Section")
```

2、继承`FormTableViewController`，即可直接使用，如：

```swift
import GZCList

class FormTableDemo: FormTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      	form +++ TableSection("Section")
    }
}
```

### 创建CollectionView

1、直接使用`FormCollectionView`创建，如：

```swift
import GZCList

let collectionView = FormCollectionView()

collectionView.form +++ CollectionSection("Section")
```

2、继承`FormCollectionViewController`，即可直接使用，如：

```swift
import GZCList

class FormCollectionDemo: FormCollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      	form +++ CollectionSection("Section")
    }
}
```

## 使用定义好的Row/Item

为了加以区分，对框架内定义好的单元格进行了命名统一：

> `TableView`对应的单元格命名统一为`XXXRow`
>
> `CollectionView`对应的单元格命名统一为`XXXItem`

为了方便使用，基类中定义了一些通用属性：

> **backgroundColor**：cell背景色
>
> **contentInsets**：内容边距（理论上应为contentView到Cell的边距，仅作为存储用，基类中未根据此属性调整，子类中可按需自行调整布局）
>
> **contentBgColor**：contentView背景色
>
> **highlightContentBgColor**：高亮时的contentView背景色
>
> **cornerRadius**：圆角（指定值）
>
> **cornerScale**：圆角（短边长度 * cornerScale）
>
> **borderWidth**：边框线条宽度（contentView）
>
> **borderColor**：边框颜色（contentView）
>
> **highlightBorderColor**：高亮时的边框颜色（contentView）
>
> **isDisabled**：是否不可编辑，默认为false，设置为true后，cell的点击事件将不会触发

以及一些回调方法(Callbacks)：

> **onChange()**：当row的`value`改变时调用。你可以在这里调整一些参数，甚至显示或隐藏其他row。
>
> **onCellSelection()**：当用户点击row并且被选中的时候调用。
>
> **onCellHighlightChanged()**：高亮状态改变时的回调，回调中可使用row的isHighlighted属性获取是否高亮状态

下面列表为定义好可直接使用的单元格：

|             名称             |                             说明                             |           效果图            |                             文档                             |
| :--------------------------: | :----------------------------------------------------------: | :-------------------------: | :----------------------------------------------------------: |
|     LabelRow & LabelItem     | 文字展示单元格</br>可展示标题和value，同时提供自定义标题、value样式和位置等属性 |    ![](./Doc/Label.png)     |     [LabelRow & LabelItem](./Doc/LabelRow&LabelItem.md)      |
|    SwitchRow & SwitchItem    | 带switch控件的单元格，可展示标题和switch，同时提供自定义标题、switch样式和位置等属性 |  ![](./Doc/SwitchRow.gif)   |   [SwitchRow & SwitchItem](./Doc/SwitchRow&SwitchItem.md)    |
|    ButtonRow & ButtonItem    | 按钮单元格（整个单元格为一个按钮），点击可以任意操作（如点击跳转到新的界面） |    ![](./Doc/Button.gif)    |   [ButtonRow & ButtonItem](./Doc/ButtonRow&ButtonItem.md)    |
|      FoldRow & FoldItem      | 可折叠的内容展示单元格，可自定义展示内容、左侧视图、展开按钮样式等 |   ![](./Doc/FoldRow.gif)    |        [FoldRow&FoldItem](./Doc/FoldRow&FoldItem.md)         |
|  FoldTextRow & FoldTextItem  | 可折叠的文字展示单元格，可自定义左侧视图、折叠按钮样式，继承于 FoldRow & FoldItem |   ![](./Doc/FoldText.gif)   | [FoldTextRow&FoldTextItem](./Doc/FoldTextRow&FoldTextItem.md) |
|     ImageRow & ImageItem     | 图片展示单元格，可设置图片预估比例、内容边距、圆角等，支持网络图片加载 |   ![](./Doc/ImageRow.gif)   |      [ImageRow&ImageItem](./Doc/ImageRow&ImageItem.md)       |
|      LineRow & LineItem      | 定义好的分割线单元格，可自定义高度(cellHeight)、圆角、内容边距、线的颜色以及背景色 |   ![](./Doc/LineRow.png)    |        [LineRow&LineItem](./Doc/LineRow&LineItem.md)         |
| TextFieldRow & TextFieldItem | 带textfield的单元格，可展示标题和输入框，同时提供自定义标题、输入框样式等属性 | ![](./Doc/TextFieldRow.png) | [TextFieldRow&TextFieldItem](./Doc/TextFieldRow&TextFieldItem.md) |
|  TextViewRow & TextViewItem  | 带textview输入框单元格，可展示左侧标题和右侧的输入框，同时提供自定义标题、输入框样式，自动调整高度 | ![](./Doc/TextViewRow.gif)  | [TextViewRow&TextViewItem](./Doc/TextViewRow&TextViewItem.md) |
|  HtmlInfoRow & HtmlInfoItem  | 带webview的单元格，用于展示html代码，会根据网页内容大小和用户设置自动调整最终展示大小 | ![](./Doc/HtmlInfoRow.png)  | [HtmlInfoRow & HtmlInfoItem](./Doc/HtmlInfoRow&HtmlInfoItem.md) |

## 自定义Row/Item

自定义单元格的方法参考这里: [如何自定义Row/Item](./Doc/如何自定义Row和Item.md)

## 内联单元格（InlineTableRow/InlineCollectionItem）

内联单元格是一个特定的单元格类型，可以在它下面（下一个单元格位置）动态的显示一个单元格。正常来说内联单元格在被点击时在展开和折叠两种状态切换。

所以，为了创建一个内联单元格，我们需要两个单元格，一个总是显示的单元格（下面简称为**RootRow**），另外一个被展开和折叠的单元格（下面简称为**OpenRow**）。

另外一个要求是，这两个单元格的value类型必须是一样的。

创建RootRow和OpenRow的方法与上面的自定义单元格方法相同。

创建完成后，**RootRow**需额外实现 `InlineTableRowType`/ `InlineCollectionItemType`协议，并关联OpenRow，如：

```swift
class InlineRootRow: TableRowOf<XXXCell>, InlineTableRowType {
    typealias InlineRow = InlineOpenRow
}
```

 `InlineTableRowType`/ `InlineCollectionItemType`协议增加并实现了几个方法：

```swift
// 配置方法，子类中实现
/// 首次显示内联Row之前配置这个将要展开的Row
func setupInlineRow(_ inlineRow: InlineRow)

// 可直接调用的方法，协议中已实现逻辑
/// 展开（打开）内联行
func expandInlineRow()
/// 折叠（关闭）内联行
func hideInlineRow()
/// 更改内联行的状态（展开/折叠）
func toggleInlineRow()

// 给使用方调用的
/// 设置展开内联行的回调
func onExpandInlineRow()
/// 设置折叠内联行的回调
func onCollapseInlineRow()
```

我们在Row的点击事件中添加调用协议方法来实现内联行的展开和收起，并在`setupInlineRow()`中配置OpenRow的值（由于OpenRow的value与RootRow相同，因此外界不需要额外的操作来修改OpenRow的值）：

```swift
class InlineRootRow: TableRowOf<XXXCell>, InlineTableRowType {
    typealias InlineRow = InlineOpenRow
  
    func setupInlineRow(_ inlineRow: InlineOpenRow) {
        inlineRow.value = "打开了"
        inlineRow.cellHeight = UITableView.automaticDimension
    }
    
    override func customDidSelect() {
        super.customDidSelect()
        if !isDisabled {
            toggleInlineRow()
        }
    }
}
```

使用方只需要添加RootRow即可：

```swift
TableSection("InlineRow")
  <<< InlineRootRow(title: "点我打开")
    .onExpandInlineRow({ (cell, rootRow, openRow) in
        print("打开了")
    })
    .onCollapseInlineRow({ (cell, rootRow, openRow) in
        print("收起了")
    })
```

## 关于CollecitonView的元素拖动

框架中定义了`CollectionMultivalusedSection`用于支持元素拖动重新排序，并已实现了长按元素拖动的逻辑，使用上与CollectionSection基本一致，效果图：

![](./Doc/拖动排序.gif)

## 关于添加Row的左(右)滑展开按钮

**TableRow**中包含了属性`trailingSwipe`和`leadingSwipe（iOS11以上可用）`，用于支持左右滑展开按钮，其实就是系统带的cell侧滑事件，使用方式如下，更多请参考Demo：

```swift
//...
// 添加左滑事件
let delete = SwipeAction(style: .destructive, title: "删除") { (action, r, handler) in
    handler?(true)
}
delete.image = UIImage(named: "delete")

let other1 = SwipeAction(style: .normal, title: "点击1") { (action, r, handler) in
    row.title = "点击了1"
    row.updateCell()
    handler?(true)
}
other1.actionBackgroundColor = .blue

let other2 = SwipeAction(style: .normal, title: "点击2") { (action, r, handler) in
    row.title = "点击了2"
    row.updateCell()
    handler?(true)
}
other2.actionBackgroundColor = .yellow

row.trailingSwipe.actions = [delete,other1,other2]
```

效果如下：

![](./Doc/侧滑事件.gif)

## 作者

Guo ZhongCheng, gzhongcheng@qq.com

## License

GZCList is available under the MIT license. See the LICENSE file for more info.
